#!/bin/python

import sys

def pow_lvl(x, y, i):
    rack_id = x + 10
    pwr = ((rack_id * y) + i) * rack_id
    return ((pwr // 100) % 10) - 5

# summed squares
serial = int(open(sys.argv[1]).read().strip())
square = 3
size = 300

# range(1, 3) = 1, 2
# range(3) = 0, 1, 2

grid = [[ pow_lvl(x, y, serial) for y in range(1, size + 1)] for x in range(1, size + 1)]
summed_area = [[ 0 for y in range(size)] for x in range(size)]

def get_sum(x, y):
    if x < 0 or y < 0:
        return 0
    else:
        return summed_area[x][y]

for x in range(size):
    for y in range(size):
        summed_area[x][y] = grid[x][y] \
            + get_sum(x - 1, y) \
            + get_sum(x, y - 1) \
            - get_sum(x - 1, y -1)

assert summed_area[0][0] == grid[0][0]
assert summed_area[0][1] == (grid[0][0] + grid[0][1])
assert summed_area[1][0] == (grid[0][0] + grid[1][0])
assert summed_area[1][1] == (grid[0][0] + grid[1][0] + grid[0][1] + grid[1][1])

max_pow = 0
max_coords = (0, 0)

for x in range(square, size - square):
    for y in range(square, size - square):
        this_pow = get_sum(x - 1, y - 1) \
            + get_sum(x - 1 + square, y - 1 + square) \
            - get_sum(x -1, y - 1 + square) \
            - get_sum(x - 1 +square, y - 1)
        if this_pow > max_pow:
            max_pow = this_pow
            # offset because arrays start at 0
            max_coords = (x + 1, y + 1)
print('{},{}'.format(max_coords[0], max_coords[1]))
