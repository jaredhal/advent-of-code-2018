#!/bin/python

import sys, re

input = open(sys.argv[1]).read().strip().splitlines()

# x, y, vx, vy
lines = [[int(n) for n in re.findall(r'-?\d+', line)] for line in input]

last_dx = 1 << 31
last_dy = 1 << 31
t = 0
min_x = 1 << 31
max_x = 0
min_y = 1 << 31
max_y = 0
last_min_x = 1 << 31
last_min_y = 1 << 31
last_max_x = 0
last_max_y = 0

while True:
	t = t + 1
	min_x = 1 << 31
	max_x = 0
	min_y = 1 << 31
	max_y = 0
	for x, y, vx, vy in lines:
		this_x = x + (vx * t)
		this_y = y + (vy * t)
		min_x = this_x if this_x < min_x else min_x
		min_y = this_y if this_y < min_y else min_y
		max_x = this_x if this_x > max_x else max_x
		max_y = this_y if this_y > max_y else max_y
	dx = max_x - min_x
	dy = max_y - min_y
	if last_dx < dx or last_dy < dy:
		t = t - 1
		break
	else:
		last_dx = dx
		last_dy = dy
		last_min_x = min_x
		last_min_y = min_y
		last_max_x = max_x
		last_max_y = max_y

print(t)
