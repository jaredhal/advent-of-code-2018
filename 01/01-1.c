#include<stdio.h>
#include<stdlib.h>

int day1(int argc, int args[]){
int results = 0;
	for(int i=0; i < argc; i++){
		results += args[i];
	}
	return results;
}

int main(int argc,char *argv[]){
	int args[argc];
	for(int i=0; i<argc; i++){
		args[i]=(int) atoi(argv[i]);
	}

	printf("%d\n",day1(argc, args));
}
