#include<stdio.h>
#include<stdlib.h>

int find(int size, int args[], int x) {
	for (int i=0; i < size; i++){
		if(args[i] == x){
			return i;
		}
	}
	return -1;
}

int day1part2(int argc, int args[]){
	int i, freq= 0;
	int size = 1;
	int *distincts = malloc(size*sizeof(int)); 
	distincts[0] = freq;
	do {
		for (int i=0; i<argc; i++){
			freq=freq + args[i];
			int index = find(size, distincts, freq);
			if(index == -1) {
				size++;
				distincts = realloc(distincts, size*sizeof(int));
				distincts[size-1] = freq;
			} else {
				return freq;
			}
		}
	}while(1);
}

int main( int argc, char *argv[] ) {
	int args[argc-1];
	for (int i = 1; i < argc; i++ ) {
		args[i-1] = (int) atoi(argv[i]);
	}
	int result = day1part2(argc-1, args);
	printf("%d\n",result);
	return result;
}
