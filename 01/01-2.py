#!/bin/python
import sys

input = open(sys.argv[1]).read().strip()
input = input.splitlines()

x = 0
results = 0
uniques = [0]

while True:
	results = results + int(input[x])
	if results in uniques:
		break
	else:
		uniques.append(results)
	if x == len(input)-1:
		x = 0
	else: 
		x += 1

print(results)
