#!/bin/python
import sys

input = open(sys.argv[1]).read().strip()
input = input.splitlines()

arr = list(input[0])

stack = []
x = 0
while x < len(arr):
	if len(stack) == 0:
		stack.append(arr[x])
		x += 1
	elif abs(ord(stack[len(stack) - 1]) - ord(arr[x])) == 32:
		stack.pop()
		x += 1
	else:
		stack.append(arr[x])
		x += 1

print(len(stack))
