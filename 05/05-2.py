#!/bin/python
import sys

input = open(sys.argv[1]).read().strip()
input = input.splitlines()

arr = list(input[0])

def collapse(plist):
	stack = []
	x = 0
	while x < len(plist):
		if len(stack) == 0:
			stack.append(plist[x])
			x += 1
		elif abs(ord(stack[len(stack) - 1]) - ord(plist[x])) == 32:
			stack.pop()
			x += 1
		else:
			stack.append(plist[x])
			x += 1
	return len(stack)

uniques = ''.join(set(arr))
results = {}

min_results = None
key = None
for char in uniques:
	results[char] = collapse(list(input[0].replace(char.lower(),'').replace(char.upper(),'')))
	if min_results == None:
		min_results = results[char]
		key = char
	elif results[char] < min_results:
		min_results = results[char]
		key = char

print(min_results)
