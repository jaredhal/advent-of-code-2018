#!/bin/python

import sys

input = open(sys.argv[1]).read().strip()
input = input.splitlines()

x_dim = 1000
y_dim = 1000

matrix = [[0 for x in range(x_dim)] for y in range(y_dim)]

for claim in input:
	parts = claim.split(' ')
	id = parts[0][1::]
	dim = parts[2].split(',')
	left = int(dim[0])
	top = int(dim[1][:-1:])
	dim = parts[3].split('x')
	width = int(dim[0])
	height = int(dim[1])

	for x in range(width):
		for y in range(height):
			matrix[x+left][y+top] += 1

overlap = 0

for x in range(x_dim):
	for y in range(y_dim):
		if matrix[x][y] > 1:
			overlap += 1

print(overlap)
