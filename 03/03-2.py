#!/bin/python

import sys
import pprint

input = open(sys.argv[1]).read().strip()
input = input.splitlines()

x_dim = 1000
y_dim = 1000

matrix = [[0 for x in range(x_dim)] for y in range(y_dim)]

for claim in input:
	parts = claim.split(' ')
	dim = parts[2].split(',')
	left = int(dim[0])
	top = int(dim[1][:-1:])
	dim = parts[3].split('x')
	width = int(dim[0])
	height = int(dim[1])

	for x in range(width):
		for y in range(height):
			matrix[x+left][y+top] += 1

unique_id = None

for claim in input:
	parts = claim.split(' ')
	id = parts[0][1::]
	dim = parts[2].split(',')
	left = int(dim[0])
	top = int(dim[1][:-1:])
	dim = parts[3].split('x')
	width = int(dim[0])
	height = int(dim[1])

	unique = True
	for x in range(width):
		for y in range(height):
			if matrix[x+left][y+top] > 1:
				unique = False
				break
	if unique is True:
		unique_id = id

print(unique_id)
