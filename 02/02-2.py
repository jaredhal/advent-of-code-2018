#!/bin/python

import sys

input = open(sys.argv[1]).read().strip()

words = input.splitlines()
word_one = None
word_two = None

for word_a in words:
	for word_b in words:
		diffs = 0
		for index, char in enumerate(word_a):
			if char != word_b[index]:
				diffs += 1
		if diffs == 1:
			word_one = word_a
			word_two = word_b

result = ""
for index, char in enumerate(word_one):
	if char == word_two[index]:
		result += char
print(result)
