#!/bin/python

import sys

input = open(sys.argv[1]).read().strip()
input = input.splitlines()

def instcount(instances, word):
	counts = {}
	for char in word:
		if char in counts:
			counts[char] += 1
		else:
			counts[char] = 1
	for key in counts:
		if counts[key] == instances:
			return True
	return False

twos = 0
threes = 0
for word in input:
	if instcount(2,word):
		twos += 1
	if instcount(3,word):
		threes += 1

print(twos*threes)
