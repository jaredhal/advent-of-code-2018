#include<stdio.h>
#include<stdbool.h>

int instances(char val, char arr[]) {
	int inst = 0;
	int len = sizeof(arr)/sizeof(char);
	for(int i = 0; i < len; i++){
		(arr[i] == val) ? inst++ : false;
	}
	return inst;
}

bool checkCount(int numeral, char *id[]) {
	int len = sizeof(id)/sizeof(char);
	for(int i = 0; i < len; i++){
		if(instances(id[i],id) == numeral){
			return true;
		}
	}
	return false;
}

int main(int argc, char *argv[]) {
	int threes, fours = 0;
	for (int i=1; i < argc; i++){
		if(checkCount(3,*argv[i])){
			threes++;
		}
		if(checkCount(4,*argv[i])){
			fours++;
		}
	}
	int result = threes*fours;
	printf("%d\n",result);
	return result;
}
