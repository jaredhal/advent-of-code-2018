#!/bin/python

import sys
from datetime import datetime

def qs_on_dim(array, dim):
	less = []
	equal = []
	greater = []

	if len(array) > 1:
		pivot = array[0]
		for x in array:
			if x < pivot:
				less.append(x)
			elif x == pivot:
				equal.append(x)
			elif x > pivot:
				greater.append(x)
		return qs_on_dim(less, dim) + equal + qs_on_dim(greater, dim)
	else:
		return array

	return qs_on_dim(array, dim, begin, end)

input = open(sys.argv[1]).read().strip()
input = input.splitlines()

time_act = []

for x in input:
	stamp = datetime.strptime(x[0:18],'[%Y-%m-%d %H:%M]')
	act = x[18::].strip()
	row = [stamp, act]
	time_act.append(row)

time_act = qs_on_dim(time_act, 0)

def add_sleep(arr, begin, end):
	for x in range(0,60):
		if begin <= x < end:
			arr[x] += 1

guards_sleeplog = {}
guards_sleeptime = {}
id = None

for x in time_act:
	stamp = x[0]
	if x[1] == "falls asleep":
		begin_sleep = stamp
	elif x[1] == "wakes up":
		end_sleep = stamp
		add_sleep(guards_sleeplog[id], begin_sleep.minute, end_sleep.minute)
		guards_sleeptime[id] += end_sleep.minute - begin_sleep.minute
	if x[1][:5] == "Guard":
		end_sleep = stamp
		id = x[1].split()[1][1:]
		if id not in guards_sleeplog:
			guards_sleeplog[id] = [0 for x in range(60)]
		if id not in guards_sleeptime:
			guards_sleeptime[id] = 0

sleep = 0
id = None

for x in guards_sleeptime:
	if guards_sleeptime[x] > sleep:
		sleep = guards_sleeptime[x]
		id = x

time_slept = 0
i = None

for index, minute in enumerate(guards_sleeplog[id]):
	if minute > time_slept:
		i = index
		time_slept = minute

print(int(id) * i)
