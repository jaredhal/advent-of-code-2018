#!/bin/python

import sys

input = open(sys.argv[1]).read().strip().split(' ')

class node:
	def __init__(self, child_count, meta_chunks):
		self.child_count = int(child_count)
		self.meta_chunks = int(meta_chunks)
		self.meta = []
		self.children = []

	def build_children(self, input):
		if self.child_count > 0:
			for c in range(0,self.child_count):
				n = node(input[0],input[1])
				input = input[2:]
				input = n.build_children(input)
				self.children.append(n)
		result = 0
		for m in range(0, self.meta_chunks):
			self.meta.append(int(input[0]))
			del input[0]
		return input

	def get_values(self):
		result = 0
		if self.child_count == 0:
			for m in self.meta:
				result += m
		else:
			for m in self.meta:
				if m > 0:
					try:
						result += self.children[m - 1].get_values()
					except IndexError:
						pass
		return result

root = node(input[0], input[1])
root.build_children(input[2:])
print(root.get_values())
