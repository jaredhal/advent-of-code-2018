#!/bin/python

import sys

input = open(sys.argv[1]).read().strip().split(' ')

def return_results(input):
	sub_nodes = int(input[0])
	meta_chunks = int(input[1])
	input = input[2:]
	results = 0
	if sub_nodes > 0:
		for i in range(0,sub_nodes):
			input, add_results = return_results(input)
			results += add_results
	for i in range(0, meta_chunks):
		results += int(input[0])
		del input[0]
	return input, results

a, results = return_results(input)

print(results)
