#!/bin/python
import sys

input = open(sys.argv[1]).read().strip()
input = input.splitlines()

max_x = 0
max_y = 0

points = []

for coord in input:
	x = int(coord.split(',')[0].strip())
	y = int(coord.split(',')[1].strip())
	max_x = x if (x > max_x) else max_x
	max_y = y if (y > max_y) else max_y
	min_y = y if (y < min_y) else min_y
	min_x = x if (x < min_x) else min_x
	points.append([x,y])

blocks = [[None for y in range(max_y + 1)] for x in range(max_x + 1)]
areas = {}

bounded_map = [True for point in points]

for y in range(-1,max_y + 2):
	for x in range(-1,max_x + 2):
		min_dist = max_x + max_y
		nearest_point = None
		for point in points:
			this_dist = abs(x - point[0]) + abs(y - point[1])
			if this_dist == min_dist:
				nearest_point = None
			elif this_dist < min_dist:
				nearest_point = points.index(point)
				min_dist = this_dist
			elif -1 < x and -1 < y and x < max_x + 1 and y < max_y + 1:
				blocks[x][y] = nearest_point
		if nearest_point in areas and nearest_point is not None:
			areas[nearest_point] += 1
		elif nearest_point is not None:
			areas[nearest_point] = 1

		if (x == -1 or y == -1) and nearest_point is not None:
			bounded_map[nearest_point] = False
		elif (x == max_x+1 or y == max_y + 1) and nearest_point is not None:
			bounded_map[nearest_point] = False

area = 0
for point in points:
	i = points.index(point)
	if areas[i] > area and bounded_map[i] is True:
		area = areas[i]

print(area)
