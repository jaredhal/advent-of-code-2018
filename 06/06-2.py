#!/bin/python
import sys 

input = open(sys.argv[1]).read().strip().splitlines()

limit = 32 if "test" in sys.argv[1] else 10000

max_x = 0
max_y = 0
min_x = 1 << 31
min_y = 1 << 31

points = []

for coord in input:
	x = int(coord.split(',')[0].strip())
	y = int(coord.split(',')[1].strip())
	max_x = x if (x > max_x) else max_x
	max_y = y if (y > max_y) else max_y
	min_x = x if (x < min_x) else min_x
	min_y = y if (y < min_y) else min_y
	points.append([x,y])

x_upper_bound = max_x + int(limit/len(points)) + 1
y_upper_bound = max_y + int(limit/len(points)) + 1
x_lower_bound = min_x - int(limit/len(points)) - 1
y_lower_bound = min_y - int(limit/len(points)) - 1

area = 0

for y in range(y_lower_bound, y_upper_bound):
	for x in range(x_lower_bound, x_upper_bound):
		dist = 0
		for point in points:
			dist += abs(x - point[0]) + abs(y - point[1])
		if dist < limit:
			area += 1

print(area)
