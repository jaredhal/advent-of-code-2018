#!/bin/python

import sys

input = open(sys.argv[1]).read().strip().split(' ')

num_of_players = int(input[0])
marbles = int(input[6])

class marble():
	def __init__(self, value, prev_marble=None, next_marble=None):
		self.value = value
		if prev_marble is None:
			self.link_left(self)
		else:
			self.link_left(prev_marble)
		if next_marble is None:
			self.link_right(self)
		else:
			self.link_right(next_marble)

	def link_left(self, marble):
		self.prev_marble = marble
		marble.next_marble = self
		return marble

	def link_right(self, marble):
		self.next_marble = marble
		marble.prev_marble = self

	def insert(self, new_marble):
		self.next_marble.link_left(new_marble)
		new_marble.link_left(self)
		return new_marble

	def pop(self):
		self.prev_marble.link_right(self.next_marble)
		self.next_marble.link_left(self.prev_marble)
		return self.value

current_marble = marble(0)
players = [0 for p in range(0,num_of_players)]
player_i = 0
last_marble = None
for m in range(1,marbles+1):
	player_i = (player_i + 1) % num_of_players
	if m % 23 == 0:
		points = m
		for i in range(0,7):
			current_marble =  current_marble.prev_marble
		temp_marble = current_marble
		current_marble = current_marble.next_marble
		points += temp_marble.pop()
		players[player_i] += points
	else:
		new_marble = marble(m)
		current_marble = current_marble.next_marble.insert(new_marble)

max = players[0]
for p in players:
	max = p if p > max else max
print(max)
