#!/bin/python

import sys
from collections import defaultdict, deque

def get_choices(nodes, completed, prereqs):
	choices = set()
	for n in nodes:
		if requires[n].issubset(completed):
			choices.update(n)
	return deque(sorted(list(choices)))

input = open(sys.argv[1]).read().strip().splitlines()
slots = 2 if 'test' in sys.argv[1] else 5
base_time = 0 if 'test' in sys.argv[1] else 60

requires = defaultdict(set)
nodes = set()

for line in input:
	a = line[5]
	b = line[36]
	nodes.update(a,b)
	requires[a].update()
	requires[b].update(a)

completed = set()
t = 0
tasks = {}

while len(nodes) + len(tasks) > 0:
	#print("t is",t)
	remove = []
	for key in tasks:
		if tasks[key] - 1 <= 0:
			completed.update(key)
			remove.append(key)
			#print("   completed",key)
		else:
			tasks[key] -= 1
			#print("   incrementing",key,"to",tasks[key])
	for key in remove:
		del tasks[key]
	choices = get_choices(nodes, completed, requires)
	for i in range(slots):
		if len(tasks) < slots and len(choices) > 0:
			#print("   from",choices,end='')
			work = choices.popleft()
			nodes.discard(work)
			tasks[work] = ord(work) - 64 + base_time
			#print("starting",work,"will take",tasks[work])
		else:
			break
	t = t + 1
print(t - 1)

## 863 is too low
