#!/bin/python

import sys 
from collections import defaultdict

input = open(sys.argv[1]).read().strip().splitlines()

requires = defaultdict(set) 
nodes = set()

for line in input:
	a = line[5]
	b = line[36]
	nodes.update(a,b)
	requires[a].update()
	requires[b].update(a)

results = []
completed = set()

while len(nodes) > 0:
	choices = set()
	for n in nodes:
		if requires[n].issubset(completed):
			choices.update(n)
	choices = sorted(list(choices))
	results.append(choices[0])
	completed.update(choices[0])
	nodes.discard(choices[0])

print(''.join(results))
